# Machine Learning notes
Notes taken during "Machine Learning" classes at Sapienza in fall 2019.

NOTE: these are HEAVILY incomplete after decision trees.

## Instructions

I've now added a precompiled pdf so you don't need to do anything unless you want to edit stuff.

### (Re)Compiling
Markdown natively compiles to html, however I find it better to convert to Latex and compile it to pdf, through [pandoc](https://pandoc.org/). Also requires a Latex engine (default is *pdflatex*, change the makefile if needed).

If you have the requirements in your PATH, you can simply run `make` from within your cloned repository.

You can also tweak pandoc's pdf conversion through the makefile if you know what you're doing.

### Editing
I currently use [Visual Studio Code](https://code.visualstudio.com/) with *Markdown all-in-one* extension, which includes KateX for rendering math environments (including the live preview).
That being said, markdown files are just text so you can edit them in anything you want and then recompile.

## Contribute
You are free (and encouraged, in the classic open-source paradigm) to add content, suggest changes or report errors.
You can:
- Directly open a **pull request** (*merge request* here on GitLab) if you're familiar with `git`
- Send me a **patch file** or even a full .md files since they're very small.
- Open an issue here on GitLab
- Simply talk to me after class :)

# Obvious Disclaimer
Since these are my personal notes, they come with no guarantee whatsoever.
