ML.pdf: src/*.md
	pandoc src/*.md -o ML.pdf --table-of-contents -V documentclass=report --number-sections -H math.tex
clean:
	rm -f ML.pdf 
	#pandoc cleans .tex files already