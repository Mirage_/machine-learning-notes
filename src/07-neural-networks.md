# Neural networks

## Biological Neural Networks

- Intelligent behaviour is an *emergent* property of a large number of simple units (neurons).
- Neurons have **switching time** (a few ms), while computer neural networks have compting cost of nano/pico-seconds. 
- Massive parallelism: bilogical NNs are in fact much more efficient than computers for complex computations.
- Each neutron has active and inactive state.
- Information is trasmitted through *neurotransmitters*.
- Synapses change size and stength with experience.
- **Hebbian learning**.

## Computer Neural Networks

- A ML algorithm that can be both supervised and unsupervised.
- The learned algorithm is an algebraic function.
- Function in linear for `Perceptron` and non-linear for `Backpropagation` algorithm.
- Features and output classes are **continuos** (real valued).
- Attempt to replicate biological neural systems
- Net function
- Threshold function (*bias*)


### Perceptron algorithm

A neural network, that is either artificial or biological, which uses weights on each neuron/perceptron in order to compute the classification of the input.
It learns a **linear** decision boundary, therefore it is not suited to all problems: if it can't be linearly separated with satisfactory results, it is probably better to use a multi-layer network instead.

`Net` computes the weighted sum of the input.

\begin{equation*}
	( \sum_{i=0}^n w_ix_i ) - T = \text{o}
\end{equation*}

> **Example**: $w_1x_1 + w_2x_2 - T = 0$

The **threshold function** classifies the instance based on the net value.

The objective is to learn the **synaptic weights** ($w_i$) such that each istance $x_j$ produces the correct output $o_j = \phi(net_j - T_j)$.

The algorithm tries to minimize classification error by *hill climbing* (**gradient descent**).

Results converge quickly for linerly separable data.

#### Perceptron training

The weights are randomly configured at the start of the training and the objective is to modify them until the output converges to the desired one.

This is the algorithm of the Perceptron(training), assuming that the weights $W$ are attributes.

Perceptron( $<x_0, x_1, ..., x_m>$ );

* Set weights and threshold to random values
* Until all istances are classified correctly:
	*  $\forall x_j = <x_j^1..x_j^n>\  \in D$, compute $o_j$:
	* if $o_j = y_j$: output is correct, do nothing.
	* if $o_j > y_j$: decrease weight/threshold on active input
	* if $o_j < y_j:$ increase weight/threshold


#### Weight modification

A way to modify the weights is the following: 

\begin{equation*}
	w_i = w_i + \Delta w_i
\end{equation*}

and

\begin{equation*}
	\Delta w_i := \eta (y_i - o_i) x_i
\end{equation*}

where:

* $\eta$: is called the **learning rate** and its only purpose is to scale the changes in weight, so it doesn't overfit and converges too quickly to this instance.
* $y_i$: is the target value, that is the correct vmodify the weights of $x_i$.
* $o_i$: is the the output of our model for the input $x_i$.
* $x_i$: the input value for the weight $w_i$.

This process continues until *every* sample is classified correctly, which means that the algorithm will ***not* converge** if the instances are *not* linearly separable.

### Activation function

An activation function dictates the form of the output of a perceptron.
The simplest example is the following binary classification:

``` python
def activation(x):
	return x > 0 ? 1 : -1
```

A commonly used function is the **sigmoid**: it closely resembles the activation function of the real neuron, and it is differentiable:

\begin{equation*}
	f(x) = \frac{1}{1 + e^-y}	
\end{equation*}

\begin{equation*}
	df(x) = f(x)*(1 - f(x))
\end{equation*}

### Error measuring

The error function for a single perceptron can be expressed as:

\begin{equation*}
	E(\vec{w}) = \frac{1}{2} \sum_{d=0}^d (y_d - o_d)^2
\end{equation*}

Where the value $o_d$ is the output for every instance $d$.

This formula is called the mean squared. The coefficient $\frac{1}{2}$ is artificially introduced simplify the derivation step: since we are interested only in the **direction** of the slopes, scaling the entire function by a costant won't affect the result.

### Gradient descent

> The **gradient** of a **scalar field** is a **vector field** in the direction of the greatest rate of increase/decrease of the scalar field [...]

> If we think the derivate as the slope of a monodimensional function, then the gradient computes the slope of a multidimensional function $\bigtriangledown \text{Hyrule}(x, y) = <\frac{\partial \text{Hyrule}(x, y)}{\partial x}, \frac{\partial \text{Hyrule}(x, y)}{\partial y}>$, in this example if the function describes the height map of Hyrule then the gradient, given a point $<x, y>$, returns the direction of the nearest peak and the slope inclination. In short we talk about derivatives when the function has only a variable, if it contains more then we use the gradient instead.

**Gradient descent** is an alternative method to update weights, which will work even when the classes are not linearly separable.
The algorithm follows a simple logic: we want to modify the weights in order to decrease the error each time. Having the formula of the error simplifies things a bit.

\begin{equation*}
	\vec{w} = \vec{w} \Delta \vec{w}
\end{equation*}

where $\Delta \vec{w}$ is 

\begin{equation*}
	\Delta \vec{w} = - \eta \bigtriangledown E(\vec{w})
\end{equation*}

The minus is to find the gradient that points downward instead of upward, so that we are decresing the error each step.

However, we need to be able to update weights one by one. To accomplish this, we can consider that:

\begin{equation*}
	\bigtriangledown E(\vec{w}) = \left[ \frac{\partial E}{\partial w_0}, \frac{\partial E}{\partial w_1}, \dots, \frac{\partial E}{\partial w_n} \right]
\end{equation*}

So we can break down the equation for a single weight $w_i$ instead of considering the whole vector of weights: 

\begin{equation*}
	w_i = w_i + \Delta w_i
\end{equation*}

and 

\begin{equation*}
	\Delta w_i = - \eta \frac{\partial E}{\partial w_i}
\end{equation*}

Let's derive the gradient:

\begin{align*}
	\frac{\partial E}{\partial w_i} &= \frac{\partial}{\partial w_i} \frac{1}{2} \sum_{d \in D} (y_d - o_d)^2 \\
	&= \frac{1}{2} \sum_{d \in D} \frac{\partial}{\partial w_i} (y_d - o_d)^2 \\
	&= \frac{1}{2} \sum_{d \in D} 2(y_d - o_d) \frac{\partial}{\partial w_i} (y_d - o_d)
\end{align*}

We know that the output of an instance is calculated this way $o_d = \vec{w} \cdot \vec{x} = \sum_{i = 0}^n w_i \cdot x_i$, then

\begin{align*}
	&= \sum_{d \in D} (y_d - o_d) \frac{\partial}{\partial w_i} (y_d - \vec{w} \cdot \vec{x}) \\
	&= \sum_{d \in D} (y_d - o_d) ( - x_{id})
\end{align*}

So in the end

\begin{equation*}
	\Delta w_i = \eta \sum_{d \in D} (y_d - o_d) ( - x_{id})
\end{equation*}

Where $x_{id}$ is the input of the weight $i$ of the instance $d$.

Although we are assured that this method will find the **local minima**, it's still computationally slow, because we need to compute every instance before updating the weights, and this must be done for every cycle.

#### Incremental gradient descent

A modified gradient descent which does not iterate until every sample is classified correctly, but only until a limited amount of iterations are done.
It discards the sum entirely and instead computes the descent per sample $d$, iteratevely.

\begin{equation*}
	w_i = w_i + \eta (y_i - o_i) x_i
\end{equation*}

This approach is *not* guaranteed to find the local minima, but it converges much faster than the non-iterative version.

## Multi-layer networks

Unlike single-layer networks, it can represent arbitrary functions, including non-linear functions.
Typically it consist of 3+ layers of perceptrons, feeding the results **forward**:

- **input** layer.
- **hidden** layer(s).
- **output** layer.

Input nodes do *not* have activation functions: they are placeholders.
Edges between nodes are weighted by $w_{ij}$ (i.e. nodes in adjacent layers $i,j$).

Target is to learn such weights, and the threshold, in order to minimize the error.

Output values are **continuous**. Weights can be updated progressively by the **backpropagation** algorithm, using a "hill-climbing" heuristic:

1. start with a random solution
2. generate one or more neighbouring solutions (i.e. weights are slightly different)
3. test for which is better
4. IF the improvement is very little $(< \varepsilon )$ THEN exit ELSE go to step 2.

This method, although correct, is also very time consuming since we need to evaluate each new point every new instance. Similarly to single layer networks, an incremental approach may be used instead for a faster but less precise computation. 

The perceptron uses linear functions to $activate$, but a concatenation of them will also produce a linear solution. To produce a non-linear classification, multi-layer networks use the **sigmoid function** instead.
A perceptron using the sigmoid function can also be called a sigmoid unit.

> **Problem**: stepwise functions are not derivable. Common solution is to use **differentiable sigmoid function** (*logistic function*).

### Error measuring

The objective is to find $W$, i.e. all $w_{ij}$ which result in the *minimum possible cost* $\texttt{Loss}(W)$, usually computed by $MSE$ (mean square error).
The difference between the single perceptron is now the error doesn't only depend of only the output of one perceptron but it depends on the error between every one on the output layer:

\begin{equation*}
	E(\vec{w}) = \frac{1}{2} \sum_{d \in D} \sum_{k \in \text{outputs}} (y_{kd} - o_{kd})^2
\end{equation*}

According to this formula the error from the output nodes is explicit, thus derivating the weight modification is simpler than the *inner* nodes.

These are the $\Delta w_{ji}$: 

\begin{equation*}
	\begin{cases}
		\Delta w_{ji} = \eta (o_j(1 - o_j)(y_j - o_j))x_{ji} &\text{if this is an output node} \\
		\Delta w_{ji} = \eta (o_j(1 - o_j)\sum_{k \in \text{outputs}} w_{jk} \delta_k)x_{ji} &\text{if this is an hidden node}
	\end{cases}
\end{equation*}

#### Derivation

To simplify things again, we are deriving the incremental gradient descent and not the original one, meaning we are dropping the sum of all instances. Thus the error is:

\begin{equation*}
	E(\vec{w}) = \frac{1}{2} \sum_{k \in outputs} (y_k - o_k)^2
\end{equation*}

Like the single perceptron we are trying to find this:

\begin{equation*}
	w_{ji} = w_{ji} + \Delta w_{ji}
\end{equation*}

where

\begin{equation*}
	\Delta w_{ji} = - \eta \frac{\partial E}{\partial w_{ji}}
\end{equation*}

And just like before the problem is to derive the gradient.

We will be using the following notation:

* $x_{ji}$ : the $i$ input to unit $j$.
* $w_{ji}$ : the weight of input $i$ for the node $j$.
* $net_j = \sum_i w_{ji}x_{ji}$ : it is the total input to the node $j$.
* $o_j$ : The output the node $j$ contributed .
* $y_j$ : the correct output for node $j$.
* $outputs$ : the output layer.
* $Downstream(j)$ : the set of nodes that directly receive as input the output of $j$ .

\begin{align*}
	\frac{\partial E}{\partial w_{ji}} &= \frac{\partial E}{\partial net_j} \frac{\partial net_j}{\partial w_{ji}} \\	
	&= \frac{\partial E}{\partial net_j} x_{ji}
\end{align*}

Remember that $\frac{\partial net_j}{\partial w_{ji}} = \frac{\partial \sum x_{jp}w_{jp}}{\partial w_{ji}} = x_{ji}$, since $x_{ji}$ is the only constant multiplying $w_{ji}$

We can start from this equation to derive the weight modification for both the inner nodes and the output ones.

#### Derivation for output units

We can start by multiplying and dividing by $\partial o_j$

\begin{equation*}
	\frac{\partial E}{\partial net_j} = \frac{\partial E}{\partial o_j}\frac{\partial o_j}{\partial net_j}
\end{equation*}

Solving term by term:

\begin{align*}
	\frac{\partial E}{\partial o_j} &= \frac{\partial}{\partial o_j}\frac{1}{2} \sum_{k \in \text{outputs}} (y_k - o_k)^2\\
\end{align*}

In the sum every term is a costant in respect to $o_j$ except where $j = k$, thus we can discard them
\begin{align*}
	&= \frac{\partial}{\partial o_j}\frac{1}{2} (y_j - o_j)^2\\
	&= \frac{1}{2}2(y_j - o_j)\frac{\partial(y_j - o_j)}{\partial o_j}\\
	&=-(y_j - o_j)
\end{align*}

The second term:

\begin{align*}
	\frac{\partial o_j}{\partial net_j} = \frac{\partial \text{sig}(o_j)}{\partial net_j} = o_j(1 - o_j)
\end{align*}

The substitution we did is because the output of the node $j$ is just the input($net_j$), in which it goes through the sigmoid function, then in the end it just resulted as the derivative of the sigmoid function.

So $\Delta w_{ji}$ for the output nodes is just

\begin{equation*}
	-(y_j - o_j)o_j(1 - o_j) x_{ji}	
\end{equation*}

#### Derivation for hidden nodes

We do *not* know the error of the inner layers, only the error on the output nodes. However, we *do* know that a **fraction of the error** on the output node must come from the **previous** layer, therefore we compute it by attributing each fraction of the error to a node influenced by it, according to the weight of the edge connecting them.

\begin{equation*}
	\frac{\partial E}{\partial net_j} = \sum_{k \in \text{Downstream(j)}} \frac{\partial E}{\partial net_k} \frac{\partial net_k}{\partial net_j}
\end{equation*}

If we define $\delta_i = - \frac{\partial E}{\partial net_i}$, then

\begin{align*}
	&= \sum_{k \in \text{Downstream(j)}} - \delta_k \frac{\partial net_k}{\partial net_j} \\	
	&= \sum_{k \in \text{Downstream(j)}} - \delta_k \frac{\partial net_k}{\partial o_j} \frac{\partial o_j}{\partial net_j}\\	
	&= \sum_{k \in \text{Downstream(j)}} - \delta_k w_{kj} \frac{\partial o_j}{\partial net_j}\\	
	&= \sum_{k \in \text{Downstream(j)}} - \delta_k w_{kj} o_j(1 - o_j)
\end{align*}

The last two steps are identical to the ones in the derivation of the output nodes.

In the end we derived the formula for $\Delta w_{ji}$ for the hidden node:

\begin{equation*}
	\Delta w_{ji} = \eta o_j(1 - o_j)\sum_{k \in \text{Downstream(j)}} w_{kj} \delta_k x_{ji} 
\end{equation*}

Notice that $\delta_k$ can be calculated from the $\Delta w_{ji}$ of the next node in line, thus computing the weight modification should start from he output nodes and then go to the input ones.

### Backpropagation algorithm

1. **forward step**: at each iteration, for each input, compute output
2. **backward step**: update weights starting from output nodes and going backward by gradient descent rule.

`Backpropagation(` $k:$ `# layers,` $H:$ `# hidden units)`
1. set weight to small random value
2. ?

Not guaranteed to converge to zero, may oscillate indefinitely or converge to a **local optima** (?). 
In practice it often converges to low error for many networks with real data, though thousands of *epochs* may be required.

**Local minima problems** (?): to avoid
- run several trials with different random weights *(random restarts)*
- use perturbations (?)

#### Initial weight

- if all are equals, they will learn the same things and never converge.
- simple base is to select uniformly in $[-\alpha,\alpha]$.

#### Momentum

$$\Delta W_{ij}(t)=- \eta Loss/\partial W_{ij} + \mu \Delta W_{ij}(t-1)$$
where $t$ is the $t$-th update for $W_{ij}$.

Note that it keeps *memory* of previous updates.

## Hidden units

Since neural networks function as a **black box**, it is not clear *what* the hidden units are. They are considered to be something capable of **recognizing subproblems** for the given model, *i.e.* some type of regularity in the data.

In practice:

- too few prevent network from adequate fitting, too many result in overfitting
- use cross-validation to determine empirically the optimal number
- number of hidden layers depend on data complexity. They increase learning time *very* quickly in traditional backpropagation, effectively making 1 or 2 the only possible options unless using **deep learning** techniques.
