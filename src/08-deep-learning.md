# Deep learning

## Hierarchical learning
Progression from low level features to high-level features through different layers. Usually contain 7-50 hidden layers.

Previous weight-propagation algorithms do not work here...
> **Vanishing Gradient Problem**
> As more layers using certain activation functions (such as sigmoid) are added, the $\delta$s close to the input gets smaller and smaller until they become negligible, and the algorithm does not converge.

**Solution**
1. Use activation functions which do not cause small derivatives (e.g. ReLu)
2. **Residual Networks**
3. Batch normalization layers

## Advantages

- Perform better in *big data* applications (but worse with little data).
- Suited for **high-performance computing** by parallelizying over GPUs.
- **ReLu** function does not have the vanishing gradient problem.
- ? 

## Disadvantages

- Highly Parametric
- Poor interpretability
- Performs poorly with small datasets (i.e. need extensive training).

## CNN
A DL algorithm designed for image processing (but has other applications too).

### Architecture
A hierarchy of *loosely* * connected layers, except the last layers which are fully connected.

[*] *only some neurons are connected.*

### Convolution

- uses a digital filter which is a small 2D weight mask (**kernel**)
- often each input pixel has 3 channels (i.e. rgb), each requiring its own convolution which then is fed into some output function
- can also have more than 3 filters
- can be extended to more dimensions
- 3D volumes ?

The system can start from random weights, since filters will somehow learn to identify features of the image (or other subject)
Compared to Dtree, introduces the concept of vicinity between nodes.
Net_j is only computed at each iteration over a small part of the input (the part under the kernel), therefore much more efficient than MLP
Introduces compression on the data (?)
In the final layer there is a different approach using **Softmax**

### Kernels
Each filter is a **feature identifier**. For images: edges, curves, shapes.

The size of a kernel is an important hyper-parameter:
- too large sizes will overlook details that could translate into useful features (too much input compression)
- too small sizes will not be able to understand the "bigger picture" because each unit won't be connected enough to the rest of the input

#### Strides and padding
Kernels usually slide by 1-unit steps, but this number (*stride*) can also be tuned to further reduce the number of connections if suited (i.e. more compression). Unlike larger kernel sides, however, this allows you to reduce the **overlap** between each kernel position, which might be desirable.
A problem with strides $>1$ is that this introduces the problem of going out of bounds at the edges, requiring **padding**, i.e. 0-value borders surrounding the input data. In order to not have any [?] 
$$|Padding| = |Kernel|-1$$

### ReLu
**Rectified Linear** *(ReLu)* was the first activation function used in CNN to remove the problem of the vanishing gradient, although there are now several variations as well as newer ones.

- derivative for negative or null vals is 0
- ?

### Pooling layer
An additional layer that performs additional compression, meant to filter out unwanted contextual information such as the particular rotation of the image, or more precisely to select *position-independent* features exclusively, e.g. by selecting the max/avg value in each sub-region [[? add ref]()].
The idea behind it is to *summarize* each sub-region of the input.
Over-pooling will however cause excessive loss of context.

### Softmax layer
The final layer, like in MLP, is fully connected to the previous one. The activation function for this layer is **Softmax**, which outputs a probability-like vector in [0,1].

### Loss function

- **MSE** (mean squared error) in internal convolutional layers, like MLP
- **Cross-Entropy** for Softmax layer

#### Cross Entropy
[def ?]

Weights are updated using gradient descent on the total cross entropy

---

### Hyper-parameters recap

- Number of filters per layer
- Dimension of filter
- Stride value
- Choice of activation function
- Learning rate $\eta$
- Pooling (which kind, or none)

---

## Stacked Denoising Auto-encoders
Minimally trained model that aims to reconstruct images from corrupted input data (i.e. that have noise).

### Encoders in NNs

- A deterministic mapping function $f_\theta$ that transforms an input vector $x$ into a *hidden* representation $y=f_\theta(x)$.
- Autoeconder training consists of minimizing the **reconstruction error**, i.e. the difference between $x$ and $g_\theta?$ where $g = f^{-1}$.
- Can be represented as a single (or multi) hidden layer neural network with equal umber of input and output nodes.
- Can be used as an **anomaly detector**: an instance that is very different from the others won't be reconstructed properly by this model.

At each layer, the network is trained until it can recreate the input from the corrupted output (i.e. until it learns a denoising function $f_\theta^i$ for each layer $i$).
Since each layer's function must be able to reconstruct the previous layer, we can achieve high data compression without losing meaningful information.
Only the first layer is trained on groundtruth (the original image) [?]